/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2017 Inria. All rights reserved.
 * @copyright (c) 2012-2015 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 *  @file chameleon_config.h
 *
 *  MORSE main header
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver,
 *  and INRIA Bordeaux Sud-Ouest
 *
 *  @version 0.9.1
 *  @author Florent Pruvost
 *  @date 2017-01-06
 *
 **/
#ifndef CHAMELEON_CONFIG_H_HAS_BEEN_INCLUDED
#define CHAMELEON_CONFIG_H_HAS_BEEN_INCLUDED

#define CHAMELEON_VERSION_MAJOR @CHAMELEON_VERSION_MAJOR@
#define CHAMELEON_VERSION_MINOR @CHAMELEON_VERSION_MINOR@
#define CHAMELEON_VERSION_MICRO @CHAMELEON_VERSION_MICRO@

/* Scheduling engine */
#cmakedefine CHAMELEON_SCHED_QUARK
#cmakedefine CHAMELEON_SCHED_PARSEC
#cmakedefine CHAMELEON_SCHED_STARPU

/* Communication engine */
#cmakedefine CHAMELEON_USE_MPI

/* GPU Support */
#cmakedefine CHAMELEON_USE_CUDA
#cmakedefine CHAMELEON_USE_CUBLAS
#cmakedefine CHAMELEON_USE_CUBLAS_V2
#cmakedefine CHAMELEON_USE_MAGMA

/* Simulating */
#cmakedefine CHAMELEON_SIMULATION


#endif  /* CHAMELEON_CONFIG_H_HAS_BEEN_INCLUDED */
