/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2016 Inria. All rights reserved.
 * @copyright (c) 2012-2014, 2016 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file codelet_zpotrf.c
 *
 *  MORSE codelets kernel
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @version 2.5.0
 * @comment This file has been automatically generated
 *          from Plasma 2.5.0 for MORSE 1.0.0
 * @author Hatem Ltaief
 * @author Jakub Kurzak
 * @author Mathieu Faverge
 * @author Emmanuel Agullo
 * @author Cedric Castagnede
 * @date 2010-11-15
 * @precisions normal z -> c d s
 *
 **/

#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"

/**
 *
 * @ingroup CORE_MORSE_Complex64_t
 *
 **/

static void cl_zpotrf_bubble_func(void *descr[], void *cl_arg)
{
    MORSE_enum uplo;
    MORSE_desc_t A;
    int Am;
    int An;
    MORSE_sequence_t sequence;
    MORSE_request_t request;

    starpu_codelet_unpack_args(cl_arg, &uplo, &A, &Am, &An, &sequence, &request);

    morse_pzpotrf(uplo, &A.subtiles[Am+An*A.lmt],
                  &sequence, &request);
    
    starpu_data_handle_t meta_handle;
    meta_handle = starpu_data_lookup((void *)STARPU_VARIABLE_GET_PTR(descr[0]));
    starpu_bubble_metadata_release(&meta_handle, 1);
}

CODELETS_CPU(zpotrf_bubble, 1, cl_zpotrf_bubble_func);

void MORSE_TASK_zpotrf(const MORSE_option_t *options,
                       MORSE_enum uplo, int n, int nb,
                       const MORSE_desc_t *A, int Am, int An, int lda,
                       int iinfo)
{
    (void)nb;
    struct starpu_codelet *codelet = &cl_zpotrf;
    void (*callback)(void*) = options->profiling ? cl_zpotrf_callback : NULL;

    MORSE_BEGIN_ACCESS_DECLARATION;
    MORSE_ACCESS_RW(A, Am, An);
    MORSE_END_ACCESS_DECLARATION;

    if(nb >= 960 && options->recur){ /* XXX: magic test value */

#define zpotrf_bubble_name 12 /* TODO: choose something sensible */
	
        static int bubble_cl_registered = 0;
	if(!bubble_cl_registered){
	    starpu_bubble_codelet_register(zpotrf_bubble_name ,&cl_zpotrf_bubble);
	    bubble_cl_registered = 1;
	}

        starpu_bubble_insert(
		zpotrf_bubble_name,
		STARPU_RW,        RTBLKADDR(A, MORSE_Complex64_t, Am, An),
		STARPU_VALUE, &uplo,		sizeof(MORSE_enum),
		STARPU_VALUE, A,		sizeof(MORSE_desc_t),
		STARPU_VALUE, &Am,		sizeof(int),
		STARPU_VALUE, &An,		sizeof(int),
		STARPU_VALUE, options->sequence,sizeof(MORSE_sequence_t),
		STARPU_VALUE, options->request,	sizeof(MORSE_request_t),
		0
		);
    } else {
    	//fprintf(stderr, "insert TASK (potrf)\n");
        starpu_bubble_task_insert(
            starpu_mpi_codelet(codelet),
            STARPU_VALUE,    &uplo,                      sizeof(MORSE_enum),
            STARPU_VALUE,    &n,                         sizeof(int),
            STARPU_RW,        RTBLKADDR(A, MORSE_Complex64_t, Am, An),
            STARPU_VALUE,    &lda,                       sizeof(int),
            STARPU_VALUE,    &iinfo,                     sizeof(int),
            /* STARPU_SCRATCH,   options->ws_worker, */
            STARPU_PRIORITY,  options->priority,
            STARPU_CALLBACK,  callback,
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
            STARPU_NAME, "zpotrf",
#endif
            0);
    }
}

#if !defined(CHAMELEON_SIMULATION)
static void cl_zpotrf_cpu_func(void *descr[], void *cl_arg)
{
    MORSE_enum uplo;
    int n;
    MORSE_Complex64_t *A;
    int lda;
    int iinfo;
    int info = 0;

    //fprintf(stderr, "TASK (potrf)\n");

    A = (MORSE_Complex64_t *)STARPU_MATRIX_GET_PTR(descr[0]);

    starpu_codelet_unpack_args(cl_arg, &uplo, &n, &lda, &iinfo);
    CORE_zpotrf(uplo, n, A, lda, &info);
}

#ifdef CHAMELEON_USE_MAGMA
static void cl_zpotrf_cuda_func(void *descr[], void *cl_arg)
{
    cudaStream_t stream[2], currentt_stream;
    MORSE_enum uplo;
    int n;
    cuDoubleComplex *A;
    /* cuDoubleComplex *hA; */
    int lda;
    int iinfo;
    int info = 0;

    A  = (cuDoubleComplex *)STARPU_MATRIX_GET_PTR(descr[0]);
    starpu_codelet_unpack_args(cl_arg, &uplo, &n, &lda, &iinfo);

    /* /\* */
    /*  *  hwork => nb*nb */
    /*  *\/ */
    /* hA = (cuDoubleComplex *)STARPU_MATRIX_GET_PTR(descr[1]); */

/*      stream[0] = starpu_cuda_get_local_stream(); */
/*      if ( cudaStreamCreate( stream+1 ) != CUDA_SUCCESS ){ */
/*          fprintf(stderr, "Error while creating stream in codelet_zpotrf\n"); */
/*          exit(-1); */
/*      } */

    CUDA_zpotrf( uplo, n, A, lda, &info);

    cudaThreadSynchronize();
/*      cudaStreamDestroy( stream[1] ); */

    return;
}
#endif
#endif /* !defined(CHAMELEON_SIMULATION) */

/*
 * Codelet definition
 */
#if defined CHAMELEON_USE_MAGMA
CODELETS(zpotrf, 1, cl_zpotrf_cpu_func, cl_zpotrf_cuda_func, 0)
#else
CODELETS_CPU(zpotrf, 1, cl_zpotrf_cpu_func)
#endif
