/**
 *
 * @copyright (c) 2009-2014 The University of Tennessee and The University
 *                          of Tennessee Research Foundation.
 *                          All rights reserved.
 * @copyright (c) 2012-2016 Inria. All rights reserved.
 * @copyright (c) 2012-2014, 2016 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved.
 *
 **/

/**
 *
 * @file codelet_ztrsm.c
 *
 *  MORSE codelets kernel
 *  MORSE is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @version 2.5.0
 * @comment This file has been automatically generated
 *          from Plasma 2.5.0 for MORSE 1.0.0
 * @author Hatem Ltaief
 * @author Jakub Kurzak
 * @author Mathieu Faverge
 * @author Emmanuel Agullo
 * @author Cedric Castagnede
 * @date 2010-11-15
 * @precisions normal z -> c d s
 *
 **/
#include "runtime/starpu/include/morse_starpu.h"
#include "runtime/starpu/include/runtime_codelet_z.h"

/**
 *
 * @ingroup CORE_MORSE_Complex64_t
 *
 **/

static void cl_ztrsm_bubble_func(void *descr[], void *cl_arg)
{
    MORSE_enum side;
    MORSE_enum uplo;
    MORSE_enum transA;
    MORSE_enum diag;
    MORSE_Complex64_t alpha;
    MORSE_desc_t A;
    int Am;
    int An;
    MORSE_desc_t B;
    int Bm;
    int Bn;
    MORSE_sequence_t sequence;
    MORSE_request_t request;

    starpu_codelet_unpack_args(cl_arg,
		    &side, &uplo,
		    &transA, &diag,
		    &alpha, &A, &Am, &An,
		    &B, &Bm, &Bn,
		    &sequence, &request);

    morse_pztrsm(side, uplo,
		    transA, diag,
		    alpha, &A.subtiles[Am+An*A.lmt],
		    &B.subtiles[Bm+Bn*B.lmt],
		    &sequence, &request);

    starpu_data_handle_t meta_handle[2];
    meta_handle[0] = starpu_data_lookup((void *)STARPU_VARIABLE_GET_PTR(descr[0]));
    meta_handle[1] = starpu_data_lookup((void *)STARPU_VARIABLE_GET_PTR(descr[1]));
    starpu_bubble_metadata_release(meta_handle, 2);
}

CODELETS_CPU(ztrsm_bubble, 2, cl_ztrsm_bubble_func);

void MORSE_TASK_ztrsm(const MORSE_option_t *options,
                      MORSE_enum side, MORSE_enum uplo, MORSE_enum transA, MORSE_enum diag,
                      int m, int n, int nb,
                      MORSE_Complex64_t alpha, const MORSE_desc_t *A, int Am, int An, int lda,
                      const MORSE_desc_t *B, int Bm, int Bn, int ldb)
{
    (void)nb;
    struct starpu_codelet *codelet = &cl_ztrsm;
    void (*callback)(void*) = options->profiling ? cl_ztrsm_callback : NULL;
    int sizeA = lda*m;
    int sizeB = ldb*n;
    int execution_rank = B->get_rankof( B, Bm, Bn );
    int rank_changed=0;
    (void)execution_rank;

    /*  force execution on the rank owning the largest data (tile) */
    int threshold;
    char* env = getenv("MORSE_COMM_FACTOR_THRESHOLD");
    if (env != NULL)
        threshold = (unsigned)atoi(env);
    else
        threshold = 10;
    if ( sizeA > threshold*sizeB ){
        execution_rank = A->get_rankof( A, Am, An );
        rank_changed=1;
    }

    MORSE_BEGIN_ACCESS_DECLARATION;
    MORSE_ACCESS_R(A, Am, An);
    MORSE_ACCESS_RW(B, Bm, Bn);
    if (rank_changed)
        MORSE_RANK_CHANGED(execution_rank);
    MORSE_END_ACCESS_DECLARATION;

    if(nb >= 960 && options->recur){ /* XXX: magic test value */

#define ztrsm_bubble_name 14 /* TODO: choose something sensible */

        static int bubble_cl_registered = 0;
        if(!bubble_cl_registered){
            starpu_bubble_codelet_register(ztrsm_bubble_name ,&cl_ztrsm_bubble);
            bubble_cl_registered = 1;
        }

        starpu_bubble_insert(
                ztrsm_bubble_name,
                STARPU_R,         RTBLKADDR(A, MORSE_Complex64_t, Am, An),
                STARPU_RW,         RTBLKADDR(B, MORSE_Complex64_t, Bm, Bn),
		STARPU_VALUE, &side,            sizeof(MORSE_enum),
		STARPU_VALUE, &uplo,            sizeof(MORSE_enum),
                STARPU_VALUE, &transA,          sizeof(MORSE_enum),
                STARPU_VALUE, &diag,            sizeof(MORSE_enum),
                STARPU_VALUE, &alpha,           sizeof(MORSE_Complex64_t),
                STARPU_VALUE, A,                sizeof(MORSE_desc_t),
                STARPU_VALUE, &Am,              sizeof(int),
                STARPU_VALUE, &An,              sizeof(int),
                STARPU_VALUE, B,                sizeof(MORSE_desc_t),
                STARPU_VALUE, &Bm,              sizeof(int),
                STARPU_VALUE, &Bn,              sizeof(int),
                STARPU_VALUE, options->sequence,sizeof(MORSE_sequence_t),
                STARPU_VALUE, options->request, sizeof(MORSE_request_t),
                0
                );
    } else {
    	//fprintf(stderr, "insert TASK (trsm)\n");
        starpu_bubble_task_insert(
//	    (int)options->sequence->schedopt,
        starpu_mpi_codelet(codelet),
        STARPU_VALUE,    &side,               sizeof(MORSE_enum),
        STARPU_VALUE,    &uplo,               sizeof(MORSE_enum),
        STARPU_VALUE,    &transA,             sizeof(MORSE_enum),
        STARPU_VALUE,    &diag,               sizeof(MORSE_enum),
        STARPU_VALUE,    &m,                  sizeof(int),
        STARPU_VALUE,    &n,                  sizeof(int),
        STARPU_VALUE,    &alpha,              sizeof(MORSE_Complex64_t),
        STARPU_R,         RTBLKADDR(A, MORSE_Complex64_t, Am, An),
        STARPU_VALUE,    &lda,                sizeof(int),
        STARPU_RW,        RTBLKADDR(B, MORSE_Complex64_t, Bm, Bn),
        STARPU_VALUE,    &ldb,                sizeof(int),
        STARPU_PRIORITY,  options->priority,
        STARPU_CALLBACK,  callback,
#if defined(CHAMELEON_USE_MPI)
        STARPU_EXECUTE_ON_NODE, execution_rank,
#endif
#if defined(CHAMELEON_CODELETS_HAVE_NAME)
        STARPU_NAME, "ztrsm",
#endif
        0);
    }
}


#if !defined(CHAMELEON_SIMULATION)
static void cl_ztrsm_cpu_func(void *descr[], void *cl_arg)
{
    MORSE_enum side;
    MORSE_enum uplo;
    MORSE_enum transA;
    MORSE_enum diag;
    int m;
    int n;
    MORSE_Complex64_t alpha;
    MORSE_Complex64_t *A;
    int lda;
    MORSE_Complex64_t *B;
    int ldb;

    //fprintf(stderr, "TASK (trsm)\n");

    A = (MORSE_Complex64_t *)STARPU_MATRIX_GET_PTR(descr[0]);
    B = (MORSE_Complex64_t *)STARPU_MATRIX_GET_PTR(descr[1]);
    starpu_codelet_unpack_args(cl_arg, &side, &uplo, &transA, &diag, &m, &n, &alpha, &lda, &ldb);
    CORE_ztrsm(side, uplo,
        transA, diag,
        m, n,
        alpha, A, lda,
        B, ldb);
}

#ifdef CHAMELEON_USE_CUDA
static void cl_ztrsm_cuda_func(void *descr[], void *cl_arg)
{
    MORSE_enum side;
    MORSE_enum uplo;
    MORSE_enum transA;
    MORSE_enum diag;
    int m;
    int n;
    cuDoubleComplex alpha;
    const cuDoubleComplex *A;
    int lda;
    cuDoubleComplex *B;
    int ldb;
    CUstream stream;

    A = (const cuDoubleComplex *)STARPU_MATRIX_GET_PTR(descr[0]);
    B = (cuDoubleComplex *)STARPU_MATRIX_GET_PTR(descr[1]);
    starpu_codelet_unpack_args(cl_arg, &side, &uplo, &transA, &diag, &m, &n, &alpha, &lda, &ldb);

    stream = starpu_cuda_get_local_stream();

    CUDA_ztrsm(
        side, uplo, transA, diag,
        m, n,
        &alpha, A, lda,
        B, ldb,
        stream);

#ifndef STARPU_CUDA_ASYNC
    cudaStreamSynchronize( stream );
#endif

    return;
}
#endif /* CHAMELEON_USE_CUDA */
#endif /* !defined(CHAMELEON_SIMULATION) */

/*
 * Codelet definition
 */
CODELETS(ztrsm, 2, cl_ztrsm_cpu_func, cl_ztrsm_cuda_func, STARPU_CUDA_ASYNC)
